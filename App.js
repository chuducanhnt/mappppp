/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator,
        StackNavigator, 
        createBottomTabNavigator, 
        createAppContainer, 
        createDrawerNavigator,
        DrawerNavigator} 
        from 'react-navigation';

import Maps from './screen/map';
import Popups from './screen/popup'

const Demonavigation = createDrawerNavigator({
  Maps: {screen: Maps, },
  Popups: {screen: Popups, },
});

const myApp = createAppContainer(Demonavigation);

export default myApp;
